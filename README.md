# Notes

This is the associated code for Horton and Chilton (2010)  ```The Labor Economics of Paid Crowdsourcing.''
The paper is available at:

1. My website: [http://john-joseph-horton.com/papers/labor_economics_of_paid_crowdsourcing.pdf](http://john-joseph-horton.com/papers/labor_economics_of_paid_crowdsourcing.pdf)
1. ACM Digital Library [http://dl.acm.org/citation.cfm?id=1807376](http://dl.acm.org/citation.cfm?id=1807376)
1. arXiv: [http://arxiv.org/pdf/1001.0627.pdf](http://arxiv.org/pdf/1001.0627.pdf)
1. SSRN: [http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1596874](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1596874)

## Citation Info

```
@inproceedings{Horton:2010:LEP:1807342.1807376,
 author = {Horton, John Joseph and Chilton, Lydia B.},
 title = {The Labor Economics of Paid Crowdsourcing},
 booktitle = {Proceedings of the 11th ACM Conference on Electronic Commerce},
 series = {EC '10},
 year = {2010},
 isbn = {978-1-60558-822-3},
 location = {Cambridge, Massachusetts, USA},
 pages = {209--218},
 numpages = {10},
 url = {http://doi.acm.org/10.1145/1807342.1807376},
 doi = {10.1145/1807342.1807376},
 acmid = {1807376},
 publisher = {ACM},
 address = {New York, NY, USA},
 keywords = {amazon's mechanical turk, crowdsourcing, human computation},
} 
```

## Replication

The repository is set up to make it transparent how the final PDF is constructed from the raw data. 
To replicate, you will need a Linux or Mac OX machine that has the following installed:

1. `R`
1. `pdflatex`
1. `make`


To replicate the data analysis, you will need several R packages.
Inspect the file `writeup/labor_supply.Rnw` to see if you are missing anything. 

####Download the repository from github
```
git clone https://bitbucket.org/johnjosephhorton/labor_economics_paid_crowdsourcing.git
```

#### Build the PDF
From `/labor_economics_of_paid_crowdsourcing`, run: 
```
cd writeup
make labor_supply.pdf
```
This should download the necessary data files and decrypt them.
It will also run the statistical analysis in R and then produce plots and tables. 
Finally, it will build the pdf file using `pdflatex`, leaving the resultant `labor_supply.pdf` in the `/writing` folder.
To see the actual steps that are being followed, you can inspect `writeup\Makefile`.

If you run into any trouble replicating, please contact me at ``john.joseph.horton@gmail.com``. 
